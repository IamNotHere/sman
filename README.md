# Serviceman
Serviceman is a small service manager for screen sessions.

<p style="text-align: center !important;">
  <img width="500" src="img/logo.svg">
</p>

## Warnings
The `GNU/Screen`, `sed`, `firejail`, `grep` and `bash` packages are required to use this script.

Cron is required to utilize the automatic start ( enable | disable ) functionality.

*!!!* **IMPORTANT** *!!!* Please bare in mind that sman **cannot differentiate** between its **own screens and your screens**, so **do not** start screens with **the same name** as any of your services.

## File structure
	.
	├── README.md
	├── backups
	├── bin
	│   ├── files
	│   │   └── service.sh
	│   ├── functions.sh
	│   ├── tools
	│   │   ├── attach.sh
	│   │   ├── autostart.sh
	│   │   ├── backup.sh
	│   │   ├── create.sh
	│   │   ├── delete.sh
	│   │   ├── disable.sh
	│   │   ├── enable.sh
	│   │   ├── help.sh
	│   │   ├── info.sh
	│   │   ├── kill.sh
	│   │   ├── list.sh
	│   │   ├── rename.sh
	│   │   ├── restart.sh
	│   │   ├── sandbox.sh
	│   │   ├── start.sh
	│   │   ├── start_attach.sh
	│   │   ├── stop.sh
	│   │   └── unlock.sh
	│   └── vars.sh
	├── services
	└── sman.sh

The bin directory is a directory for bin and resources,
the `vars.sh` file is for variables, you can change the name of the directory that contains your services (`services` by default).
The files directory contains files used by the main script, do not edit or delete them.
And the tools directory contains the modules used to execute various functions of Serviceman.

The `service` directory will contain all your services, the name of this directory can be changed in the `bin/vars.sh` file.
When you create a service using the create functionality it will create a directory for the new service inside `services/`.
When your service will get created its directory will look like this:

	myservice
	├── .created
	├── .modified
	├── .start_cmd
	├── .stop_cmd
	├── .used
	├── files
	│   └── <your service files>
	└── service.sh
	(Optionally)
	└── .service_autostart

The `.service_autostart` file only appears if the service is enabled and serves as a marker. You can manually add or remove this file and it will change weather the service is enabled.

The directory `files` should contain the files that your service consists of, its from this directory that the start command is executed.

Files `.created`, `.modified`, `.start_cmd`, `.stop_cmd` and `.used` are uses to store data and settings of the service.

The `.start_cmd` and `.stop_cmd` can be freely modified to edit the start and stop commands of the service

The `service.sh` file should not be modified.

The `backups` directory will contain backups

## Usage
*!!!* **IMPORTANT** *!!!* From this point on this file will refer to `./sman.sh` as `sman` as thats the alias the script can automatically setup for you to make it easier to use.

### Setup
In order to setup this alias type `./sman.sh help command` and follow instructions on screen.

If you want the automatic startup of automatic services to work you need to type `sman help autostart` and follow instructions on screen.

### Creating a service
To create a service use `sman create`, the syntax is:

	sman create service_name "command to start" "command to stop"

**Do not use spaces in the service name**

**No service can be named "all"**

If the command to stop your service is just CTRL + C type `^C` in the stop command field.

After this command is ran it will print out the service`s path, copy your service files into that path (the start command is executed from this path).

### Deleting a service
First make sure you actually want to delete a service (this INCLUDES *ALL* its files !)

When youre sure you want a service gone use:

	sman delete service_name service_name

The `service_name` is repeated twice to make sure you dont delete anything accidentally.

### Defunct services
Defunct services are services that are missing some core files, sman wont prevent you from executing commands on it but be careful when doing so. And dont be surprised if errors occur.

### Starting and stopping services
- To start a service:

		sman start service_name

	or list many services:

		sman start service_name service_name service_name ...

- To start all services:

		sman start all

- To stop a service:

		sman stop service_name

	or list many services:

		sman stop service_name service_name service_name ...

- To stop all services:

		sman stop all

- To KILL a service when its not responding use:

		sman kill service_name
	
	This will kill the process inside the screen session. this command will use the SIGKILL signal

#### Start and attach
To start and immediately attach to it use:

	sman starta service_name

or:

	sman sa service_name

### Restarting a service

	sman restart service_name

Keep in mind that when the service is restarting you cannot interact with it, this includes: starting, stopping, restarting, deleting.

When the service is done restarting it will unlock. In the case that it doesnt, use

	sman unlock service_name

### Ranaming a service
To rename a service use

	snam rename service_name another_service

### Backuping a service
- To backup a service use

		sman backup perform service_name

	or

		sman backup p service_name

- To list backups available for a service

		sman backup list service_name

	or

		sman backup l service_name

- To list all backups available

		sman backup list

	or

		sman backup l

*NOTE:* `sman backup` is eqiuvalent to `sman b`

### Enabling and disabling services
Enabled services will be automatically started when your system boots.

- To enable a service:

		sman enable service_name

	or list many services:

		sman enable service_name service_name service_name ...

- To disable a service:

		sman disable service_name

	or list many services:

		sman disable service_name service_name service_name ...

*NOTE:* disable and enable deliberately do not support the "all" wildcard

### Sandboxing services
Serviceman can use firejail to deny the service access to any directory other the one containing it.

In order to toggle sandboxing (by default sandboxing is off) use:

	sman sand service_name

### Displaying information about a service
To display all information about a service type:

	sman info service_name
or

	sman i service_name

#### Example info page
	Service: example_service

	State:          Stopped
	Enabled:        No
	Sandboxed:      No
	Locked:         No

	Location: 
	/home/user/service_manager/services/example_service

	Details: 
	Created:                2077-04-02 16:42:03
	Last Used:              2077-04-02 20:36:12
	Last Modified:          2077-04-02 20:36:12
	Start command:          ./run.sh
	Stop command:           ^C

	Files: 
	-rw-rw-r-- 1 user user  220 Apr  2 20:31 EXAMPLE.md
	-rw-rw-r-- 1 user user  311 Apr  2 20:38 data.txt
	-rwxrwxr-x 1 user user  289 Apr  2 20:31 run.sh

### Tools
Serviceman has its functions written as "tools" in bin/tools, this command lists them:

	sman tools

#### Help page contents
	Syntax: sman <args>

	ARGUMENTS:
        help | h                Display this help message
                                        Type "help autostart" to enable the autostart functionality
                                        Type "help command" to setup global command

        list | ls               Display list of available servicess

        info | i                Display information about a service

        attach | a              Attach to the service's screen
                                        Usage: attach <service name>
                                        or when only 1 service is running no argument is needed
                                        Use "CTRL + a" then "d" to detach

        start                   Start service, "start all" to start all services

        starta | sa             Start and attach to a service in one command

        rename                  Rename a service

        restart                 Restart a service, the service will be locked for the time it takes to restart it

        stop                    Stop service, "stop all" to stop all services

        enable                  Enable service for automatic onboot startup, "enable all" to enable all services 

        disable                 Disable service from automatic startup, "disable all" to disable all services 

        kill                    Kill a service, this command uses SIGKILL (-9) to kill the service process.
                                        Usage: kill <service name>

        create                  Create another service, usage: create <service name> "start command" "stop command"
                                        You MUST use " " when specifying start and stop commands, DO NOT use spaces in <service name>
                                        Use "^C" if the exit command/action is CTRL+C

        delete                  Delete an existing service, usage: delete <service name> <service name>
                                        The service name is repeated twice to make sure you want to delete it

        backup | b              Backup a service
                                        Usage:

                                        h | help                Displays backup specific help message

                                        list | l                Lists available backups

                                        perform | p             Performs a backup

        unlock                  Forecefully unlock a service in the event that 
                                        for some reason serviceman is not able to unlock it by itself

        sand                    Toggle firejail sandboxing for service.
                                        Usage: sand <service name>

        tools                   List loaded tools

	EXAMPLES:
			create clock_service "watch -n 1 date" "^C"
			enable clock_service
			start clock_service
			disable clock_service
			stop all
			delete clock_service clock_service
