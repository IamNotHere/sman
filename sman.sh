#!/usr/bin/env bash

# Do not edit theese variables
SCRIPT_DIR="$(dirname $(readlink -f $0))"
CONFIG_DIR="$SCRIPT_DIR/bin"
FILES_DIR="$CONFIG_DIR/files"
SERVICES_DIR="$SCRIPT_DIR/services"
BACKUPS_DIR="$SCRIPT_DIR/backups"

# Check if dependencies are met
function dependency_check() {
	local PACKAGES=(bash screen wc basename dirname readlink head tail sed grep firejail)
	local MISSING=""
	for i in "${PACKAGES[@]}"; do
		if [ "$(command -v $i)" == "" ]; then
			MISSING="$MISSING $i"
		fi
	done
	if [ "$MISSING" != "" ]; then
		if [ "$(echo $MISSING | wc -w)" == "1" ]; then
			echo "Missing package:$MISSING"
		else
			echo "Missing packages:$MISSING"
		fi
		exit
	fi
}
dependency_check

# Check config directory
if [ ! -d $CONFIG_DIR ]; then
	echo "Bin directory not present, exiting"
	exit
fi
# Load vars.sh
if [ -f "$CONFIG_DIR/vars.sh" ]; then
	source "$CONFIG_DIR/vars.sh"
else
	echo "Failed to load vars.sh"
	exit
fi
# Load functions.sh
if [ -f "$CONFIG_DIR/functions.sh" ]; then
	source "$CONFIG_DIR/functions.sh"
else
	echo "Failed to load functions.sh"
	exit
fi
# Load all tools
for i in $CONFIG_DIR/tools/*.sh; do
	source $i
done
# Ensure that SERVICES_DIR exists
if [ ! -d "$SERVICES_DIR" ]; then
	mkdir $SERVICES_DIR
fi

function menu_list_tools() {
	local LS=$(ls $CONFIG_DIR/tools | sed "s/.sh//g")
	echo "Total: $(echo $LS | wc -w)"
	echo ""
	echo ${LS^^} | sed "s/ /\n/g"
	exit
}

# Argument interpreter
if [ $# == "0" ]; then
	echo "No arguments given, use \"help\" or \"h\" for help"
	exit
fi
case $1 in
	h | help)
		menu_help "$2"
		exit
		;;
	ls | list)
		menu_list
		exit
		;;
	i | info)
		menu_info "$2"
		exit
		;;
	create)
		menu_create "$2" "$3" "$4"
		exit
		;;
	delete)
		menu_delete "$2" "$3"
		exit
		;;
	start)
		menu_start "$@"
		exit
		;;
	stop)
		menu_stop "$@"
		exit
		;;
	enable)
		menu_enable "$@"
		exit
		;;
	disable)
		menu_disable "$@"
		exit
		;;
	a | attach)
		menu_attach "$2"
		exit
		;;
	r | restart)
		menu_restart "$2"
		exit
		;;
	kill)
		menu_kill "$2"
		exit
		;;
	sand)
		menu_sandbox "$2" "$3"
		exit
		;;
	unlock)
		menu_unlock "$2"
		exit
		;;
	b | backup)
		menu_backup "$2" "$3"
		exit
		;;
	sa | starta)
		menu_start_attach "$2"
		exit
		;;
	rename)
		menu_rename "$2" "$3"
		exit
		;;
	tools)
		menu_list_tools
		exit
		;;
	perform-autostart)
		menu_autostart
		exit
		;;
	*)
		echo "Invalid argument, use \"help\" or \"h\" for help"
		exit
		;;
esac
