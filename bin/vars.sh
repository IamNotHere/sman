#!/usr/bin/env bash

# Help message
MSG_HELP="Syntax: sman <args>

ARGUMENTS:
	help | h		Display this help message
					Type \"help autostart\" to enable the autostart functionality
					Type \"help command\" to setup global command
					
	list | ls		Display list of available servicess

	info | i		Display information about a service

	attach | a		Attach to the service's screen
					Usage: attach <service name>
					or when only 1 service is running no argument is needed
					Use \"CTRL + a\" then \"d\" to detach

	start			Start service, \"start all\" to start all services

	starta | sa		Start and attach to a service in one command

	rename			Rename a service

	restart			Restart a service, the service will be locked for the time it takes to restart it

	stop			Stop service, \"stop all\" to stop all services

	enable			Enable service for automatic onboot startup, \"enable all\" to enable all services 

	disable			Disable service from automatic startup, \"disable all\" to disable all services 

	kill			Kill a service, this command uses SIGKILL (-9) to kill the service process.
					Usage: kill <service name>

	create			Create another service, usage: create <service name> \"start command\" \"stop command\"
					You MUST use \" \" when specifying start and stop commands, DO NOT use spaces in <service name>
					Use \"^C\" if the exit command/action is CTRL+C

	delete			Delete an existing service, usage: delete <service name> <service name>
					The service name is repeated twice to make sure you want to delete it

	backup | b		Backup a service
					Usage:
					
					h | help		Displays backup specific help message

					list | l 		Lists available backups

					perform | p		Performs a backup

	unlock			Forecefully unlock a service in the event that 
					for some reason serviceman is not able to unlock it by itself
	
	sand			Toggle firejail sandboxing for service.
					Usage: sand <service name>

	tools			List loaded tools

EXAMPLES:
	create clock_service \"watch -n 1 date\" \"^C\"
	enable clock_service
	start clock_service
	disable clock_service
	stop all
	delete clock_service clock_service
"