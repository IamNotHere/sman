#!/usr/bin/env bash

# Get PID of process in screen
function fn_get_pid() {
	local SCREEN_PID=$(screen -ls | tail -n +2 | head -n -1 | grep -Ewo "[0-9]*\.$1" | sed "s/[^0-9]//g")
	if [ "$SCREEN_PID" != "" ]; then
		local PROCESS_PID=$(ps --ppid $SCREEN_PID --no-headers | sed "s/ //g" | grep -o "^[0-9]*")
		echo $PROCESS_PID
	fi
}

function fn_is_running() {
	screen -ls | tail -n +2 | head -n -1 | grep -w "$1" | wc -l
}