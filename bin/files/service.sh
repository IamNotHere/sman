#!/usr/bin/env bash

# MANAGED BY SERVICE MANAGER
# DO NOT DELETE THIS FILE
# DO NOT EDIT THIS FILE

function loadFile() {
	# Check if files exist
	if [ -f "$1" ]; then
		echo $(cat $1)
	else
		echo "Failed to load var file: $1"
		exit
	fi
}

START_COMMAND=$(loadFile ".start_cmd")
STOP_COMMAND=$(loadFile ".stop_cmd")

SERVICE_NAME="$(basename $(dirname $(readlink -f $0)))"

if [ "$#" == "0" ]; then 
	exit
fi

if [ -f ".sandbox" ]; then
	SANDBOX="firejail --blacklist=/ --whitelist=$(dirname $(readlink -f $0))/files"
else
	SANDBOX=""
fi

# Enter the service directory
cd files

case $1 in
start)
	screen -S "$SERVICE_NAME" -d -m bash -c "$SANDBOX $START_COMMAND"
	;;
stop)
	screen -S "$SERVICE_NAME" -X stuff "$STOP_COMMAND"^M
	;;
*)
	exit
	;;
esac