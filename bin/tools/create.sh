#!/usr/bin/env bash

# Create a service
function menu_create() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if service.sh manager file exists in /$CONFIG_DIR/$FILES_DIR
	cd $SCRIPT_DIR
	if [ -f "$FILES_DIR/service.sh" ]; then
		# $1 = service name
		# $2 = start command
		# $3 = stop command

		if [ "$3" == "" ]; then
			echo "Insufficient arguments, use help or h for help"
			exit
		fi

		if [ "$1" == "all" ]; then
			echo "\"all\" is a reserved key name, cannot create a servce with this name"
			exit
		fi

		# Check if $SERVICES_DIR exists
		if [ "$SERVICES_DIR" != "" ]; then
			local SERVICE_DIR="$SERVICES_DIR/$1"

			# Check if service already exists
			cd "$SERVICES_DIR"
			if [ -d "$1" ]; then
				echo "Service $1 already exists"
				exit
			fi

			# Create new service
			mkdir -p $SERVICE_DIR
			cp "$FILES_DIR/service.sh" "$SERVICE_DIR/"
			chmod +x "$SERVICE_DIR/service.sh"

			cd "$SERVICE_DIR"

			# Create all var files
			touch .created
			touch .modified
			touch .used
			echo $(date +"%F %H:%M:%S") >.created
			echo $(date +"%F %H:%M:%S") >.modified
			echo $(date +"%F %H:%M:%S") >.used

			touch .start_cmd
			touch .stop_cmd
			echo "$2" > .start_cmd
			echo "$3" > .stop_cmd

			# Create files directory
			mkdir files

			# Tell user where the new service is
			echo "Service $1 resides at: "
			echo "$SERVICE_DIR/files"

			exit
		fi

	else
		echo "Service file $CONFIG_DIR/files/service.sh does not exist, can't create service"
	fi
	exit
}