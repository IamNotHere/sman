#!/usr/bin/env bash

# Stop service
function menu_stop() {
	# Get rid of 1st argument
	shift

	# Check if argument is supplied
	if [ "$#" == "0" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if user wants to affect all services
	if [ "$1" == "all" ]; then
		cd "$SERVICES_DIR"
		for i in *; do
			# Determine if service is running
			local RUNNING="$(screen -ls | grep -w "$i" | wc -l)"

			if [ "$RUNNING" != "0" ]; then
				cd "$i"
				./service.sh "stop"
			fi

			cd "$SERVICES_DIR"
		done
		exit
	fi
	
	# Handle lists
	for i in $@; do
		# Check if service exists
		if [ ! -d "$SERVICES_DIR/$i" ]; then
			echo "Service $i does not exist"
		else
			# Check if LOCK is enabled
			if [ -d "$SERVICES_DIR/$i/.lock" ]; then
				echo "Service $i lock enabled, failed to start service"
			else
				SERVICE_DIR="$SERVICES_DIR/$i"

				# Check if service already running
				local RUNNING="$(screen -ls | tail -n +2 | head -n -1 | grep -w "$i" | wc -l)"
				if [ "$RUNNING" == "0" ]; then
					echo "Service $i is not running"
				else
					cd $SERVICE_DIR
					./service.sh "stop"
				fi
			fi
		fi
	done
	exit
}
