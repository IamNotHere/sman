#!/usr/bin/env bash

# Disable service
function menu_disable() {
	# Get rid of 1st argument
	shift

	# Check if argument is supplied
	if [ "$#" == "0" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if user wants to affect all services
	if [ "$1" == "all" ]; then
		cd "$SERVICES_DIR"
		for i in *; do
			
			cd $i
			if [ -f ".service_autostart" ]; then
				rm .service_autostart
			fi

			# Update .modified
			echo $(date +"%F %H:%M:%S") > .modified
			cd "$SERVICES_DIR"
		done
		exit
	fi

	# Handle lists
	for i in $@; do
		# Check if service exists
		if [ ! -d "$SERVICES_DIR/$i" ]; then
			echo "Service $i does not exist"
		else
			cd "$SERVICES_DIR/"
			if [ -d "$i" ]; then
				cd $i
				if [ -f ".service_autostart" ]; then
					rm .service_autostart
				fi

				# Update .modified
				echo $(date +"%F %H:%M:%S") > .modified
			fi
		fi
	done
}