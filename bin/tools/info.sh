#!/usr/bin/env bash

# Get information abut a service
function menu_info() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if service exists
	if [ -d "$SERVICES_DIR/$1" ]; then
		local SERVICE_DIR="$SERVICES_DIR/$1"
		cd $SERVICE_DIR

		echo "Service: $1"
		echo ""

		# Determine if service is running
		local RUNNING="$(fn_is_running $1)"
		if [ "$RUNNING" == "0" ]; then
			echo -e "State:		\e[1;31mStopped\e[0m"
		else
			echo -e "State:		\e[1;32mRunning\e[0m"
		fi

		# Determine if service is enabled
		if [ -f ".service_autostart" ]; then
			echo -e "Enabled:	\e[1;35mYes\e[0m"
		else
			echo -e "Enabled:	\e[1;36mNo\e[0m"
		fi

		# Detect .sandbox
		if [ -f ".sandbox" ]; then
			echo -e "Sandboxed:	\e[1;35mYes\e[0m"
		else
			echo -e "Sandboxed:	\e[1;36mNo\e[0m"
		fi
		# Detect .lock
		if [ -f ".lock" ]; then
			echo -e "Locked:		\e[1;35mYes\e[0m"
		else
			echo -e "Locked:		\e[1;36mNo\e[0m"
		fi
		echo

		echo "Location: "
		echo -e "\e[4m$SERVICES_DIR/$1\e[0m"
		#echo -e "\e[4m$~/sman/services/$1\e[0m"
		echo

		echo "Details: "
		echo "Created: 		$(cat .created)"
		echo "Last Used: 		$(cat .used)"
		echo "Last Modified: 		$(cat .modified)"
		echo -e "Start command: 		\e[1;32m$(cat .start_cmd)\e[0m"
		echo -e "Stop command: 		\e[1;31m$(cat .stop_cmd)\e[0m"
		echo

		if [ -d "files" ]; then
			cd files
			echo "Files: "
			if [ "$(LANG=C ls -l)" != "total 0" ]; then
				LANG=C ls -la | sed "1,3d"
			else
				echo "No files"
			fi
			echo
		fi
	else
		echo "Service $1 does not exist"
	fi

	exit
}
