#!/usr/bin/env bash

# List services and their state
function menu_list() {
	cd "$SERVICES_DIR"
	for i in $(ls | sed "s/all//g"); do

		# Check if service.sh and var files exist
		if [ -f "$i/service.sh" -a -f "$i/.start_cmd" -a -f "$i/.stop_cmd" -a -d "$i/files" ]; then

			# Determine if service is running
			local STATE="$(fn_is_running $i)"
			if [ "$STATE" == "0" ]; then
				STATE="[\e[1;31mStopped\e[0m]"
			else
				STATE="[\e[1;32mRunning\e[0m]"
			fi

			# Determine if service is enabled
			if [ -f "$i/.service_autostart" ]; then
				STATE="[\e[1;35mEnabled\e[0m]  $STATE"
			else
				STATE="[\e[1;36mDisabled\e[0m] $STATE"
			fi

			# Detect LOCK
			if [ -f "$i/.lock" ]; then
				STATE="$STATE [\e[1;34mLOCKED\e[0m]"
			fi

			# Detect SANDBOX
			if [ -f "$i/.sandbox" ]; then
				STATE="$STATE [\e[1;33mSANDBOXED\e[0m]"
			fi

			echo -e "- $STATE $i"
		else
			echo -e "- [\e[1;31m!DEFUNCT!\e[0m] $i"
			# echo
		fi

	done
	cd "$SCRIPT_DIR"
	exit
}