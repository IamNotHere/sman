#!/usr/bin/env bash

# Rename service
function menu_rename() {
	# $1 service name
	# $2 target name

	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	if [ "$2" == "" ]; then
		echo "No name supplied"
		exit
	fi

	# Check if .lock is enabled
	if [ -f "$SERVICES_DIR/$1/.lock" ]; then
		echo "Lock enabled, failed to rename service"
		exit
	fi

	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
	fi

	# Check if names differ
	if [ "$1" != "$2" ]; then
		# Check if service exists
		if [ -d "$SERVICES_DIR/$1" ]; then
			# Check for name overlap
			if [ -d "$SERVICES_DIR/$2" ]; then
				echo "Service with the name $2 already exists"
				exit
			fi

			# Check if service is running
			local RUNNING="$(fn_is_running $1)"
			if [ "$RUNNING" == "0" ]; then
				mv "$SERVICES_DIR/$1" "$SERVICES_DIR/$2"
			else
				echo "Stop the $1 service first"
				exit
			fi
			echo "Service $1 has been renamed to $2"
		fi

		exit
	else
		echo "Confirmation failed, arguments not matching"
		exit
	fi
	exit
}