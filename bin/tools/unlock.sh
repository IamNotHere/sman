#!/usr/bin/env bash

# Unlock a service
function menu_unlock() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
		exit
	fi

	cd "$SERVICES_DIR/$1"
	if [ -f ".lock" ]; then
		rm .lock
		echo "Lock for service $1 removed"
	fi

	exit
}