#!/usr/bin/env bash

# Start and attach to service
function menu_start_attach() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if user wants to affect all services
	if [ "$1" == "all" ]; then
		echo "\"all\" is not supported by start & attach"
		exit
	fi
	
	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
	else
		# Check if LOCK is enabled
		if [ -d "$SERVICES_DIR/$1/.lock" ]; then
			echo "Service $1 lock enabled, failed to start service"
		else
			local SERVICE_DIR="$SERVICES_DIR/$1"
			cd "$SERVICE_DIR"

			# Check if service already running
			local RUNNING="$(screen -ls | tail -n +2 | head -n -1 | grep -w "$1" | wc -l)"
			if [ "$RUNNING" != "0" ]; then
				echo "Service $1 is already running"
				screen -r $1
			else
				cd $SERVICE_DIR
				./service.sh "start"
				screen -r $1

				# Update .used
				echo $(date +"%F %H:%M:%S") > .used
			fi
		fi
	fi
	exit
}
