#!/usr/bin/env bash

# Kill service process
function menu_kill() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
		exit
	fi

	RUNNING="$(fn_is_running $1)"
	if [ "$RUNNING" == "0" ]; then
		echo "Service $1 is already stopped"
	else
		kill -9 $(fn_get_pid "$1")
	fi

	exit
}
