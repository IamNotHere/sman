#!/usr/bin/env bash

# Attach to service
function menu_attach() {
	# Check if user provided a service name
	if [ "$1" == "" ]; then
		# Check how many screens are running
		local SCREENS="$(screen -ls | head -n -2 | wc -l)"
		if [ "$SCREENS" == "1" ]; then
			screen -r
		else
			echo "Specify a service"
		fi
		exit
	fi

	# Check if service exists
	if [ -d "$SERVICES_DIR/$1" ]; then
		local SERVICE_DIR="$SERVICES_DIR/$1"

		# Check if service already running
		local RUNNING="$(fn_is_running $1)"
		if [ "$RUNNING" != "0" ]; then
			# Attach
			screen -r $1
			exit
		else
			echo "Service $1 is not running"
		fi
	else
		echo "Service $1 does not exist"
	fi
}