#!/usr/bin/env bash

# Start services marked for autostart
function menu_autostart() {
	cd "$SERVICES_DIR"
	for i in *; do
		# Check if service.sh and other files exist
		if [ -f "$i/service.sh" -a -f "$i/.start_cmd" -a -f "$i/.stop_cmd" -a -d "$i/files" ]; then
			# Determine is service has autostart enabled (has .service_autostart file)
			if [ -f "$i/.service_autostart" ]; then
				# Determine if service is running
				local RUNNING="$(fn_is_running $i)"
				if [ "$RUNNING" == "0" ]; then
					cd $i
					./service.sh "start"
					
					# Update .used
					echo $(date +"%F %H:%M:%S") > .used
					cd "$SERVICES_DIR"
				fi
			fi
		fi
	done
	cd "$SCRIPT_DIR"
	exit
}