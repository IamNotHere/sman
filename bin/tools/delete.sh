#!/usr/bin/env bash

# Delete service
function menu_delete() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	# Check if .lock is enabled
	if [ -f "$SERVICES_DIR/$1/.lock" ]; then
		echo "Lock enabled, failed to delete service"
		exit
	fi

	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
		exit
	fi

	# Check if deleteion conforimation is valid
	if [ "$1" == "$2" ]; then
		# Check if service exists
		if [ -d "$SERVICES_DIR/$1" ]; then
			# Check if service is running
			local RUNNING="$(fn_is_running $1)"
			if [ "$RUNNING" == "0" ]; then
				rm -r "$SERVICES_DIR/$1"
			else
				echo "Stop the $1 service first"
				exit
			fi
			echo "Service $1 has been deleted"
		else
			echo "Service $1 does not exist"
		fi

		exit
	else
		echo "Confirmation failed, arguments not matching"
		exit
	fi
	exit
}