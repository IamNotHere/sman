#!/usr/bin/env bash

# Toggle sandboxing for service
function menu_sandbox() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	if [ ! -d "$SERVICES_DIR/$1" ]; then
		echo "Service $1 does not exist"
		exit
	fi

	if [ "$(fn_is_running $1)" != "0" ]; then
		echo "Cannot toggle sandboxing while service is running"
		exit
	fi

	cd "$SERVICES_DIR/$1"

	if [ -f .sandbox ]; then
		rm .sandbox
		echo "Sandboxing disabled for service $1"
	else
		touch .sandbox
		echo "Sandboxing enabled for service $1"
	fi

	exit
}
