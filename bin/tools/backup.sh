#!/usr/bin/env bash

# Bacup a service
function menu_backup() {
    # $1 - action
    # $2 - service name
    cd "$SERVICES_DIR"

    # Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No arguments supplied"
		exit
	fi

	if [ ! -d "$BACKUPS_DIR" ]; then
		mkdir $BACKUPS_DIR
	fi

	# Interpret sub argument
	case $1 in
	h | help)
		echo "Syntax: sman backup <action> <service_name>"
		echo "	sman b <action> <service_name>"
		echo ""
		echo "ARGUMENTS:
	list | l 		Lists available backups
					Usage:
					sman backup list - lists which services are backed up
						and how many backups are available
					sman backup list <service_name> - lists backups available for service
	
	perform | p		Performs a backup
	"
		exit
		;;
	l | list)
		if [ "$2" != "" ]; then
			# List $2 service backups
			if [ -d "$BACKUPS_DIR/$2" ]; then
				cd "$BACKUPS_DIR/$2"
				ls
			else
				echo "No backups available for service $2"
			fi
		else
			# List all services backups
			cd $BACKUPS_DIR
			for i in $(ls); do
				if [ "$(ls $i | wc -l)" != "0" ]; then
					echo - $i : $(ls $i | wc -l)
				fi
			done
		fi
		exit
		;;
	p | perform)
		if [ ! -d "$SERVICES_DIR/$2" ]; then
			echo "Service $2 does not exist"
			exit
		fi

		local RUNNING="$(fn_is_running $2)"
		if [ ! "$RUNNING" == "0" ]; then
			echo "Cant backup while service is running"	
			exit
		fi

		local BACKUP_DIR="$BACKUPS_DIR/$2"
		local BACKUP_FILE="$BACKUP_DIR/$( date +%Y-%m-%d_%H-%M-%S )"

		if [ ! -d "$BACKUPS_DIR/$2" ]; then
			mkdir "$BACKUPS_DIR/$2"
		fi

		cd "$SERVICES_DIR/$2"

		tar -czvf $BACKUP_FILE.tar.gz *
		
		exit
		;;
	*)
		echo "Invalid argument \"$1\""
		exit
		;;
	esac
}
