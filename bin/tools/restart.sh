#!/usr/bin/env bash

# Restart a service
function menu_restart() {
	# Check if argument is supplied
	if [ "$1" == "" ]; then
		echo "No argument supplied"
		exit
	fi

	cd "$SERVICES_DIR/"

	if [ ! -d "$1" ]; then
		echo "Service $1 does not exist"
		exit
	fi

	# Check if LOCK is enabled
	if [ -f "$1/.lock" ]; then
		echo "Lock enabled, failed to restart service"
		exit
	fi

	cd $1
	# Check if service is running
	local RUNNING="$(fn_is_running $1)"
	if [ "$RUNNING" == "0" ]; then
		echo "Service $1 is not running"
		exit
	fi

	./service.sh "stop"
	# Lock service when restarting
	touch .lock
	while true; do
		local VAR=$(fn_get_pid "$1")
		if [ "$VAR" == "" ]; then
			./service.sh "start"
			if [ -f ".lock" ]; then
				rm .lock
			fi
			break
			exit
		fi
		sleep 1
	done &

	# Update .used
	echo $(date +"%F %H:%M:%S") > .used
	exit
}